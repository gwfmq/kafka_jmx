FROM wurstmeister/kafka:latest

ADD kafka-0-8-2.yml /prom-jmx-conf.yml
ADD http://central.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent/0.10/jmx_prometheus_javaagent-0.10.jar /jmx_prometheus_javaagent.jar
